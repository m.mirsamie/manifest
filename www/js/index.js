document.addEventListener("backbutton", backPress, false);
function backPress(e)
{
    if(side_open)
    {
        myApp.closePanel();
    }
    else
    {
        //var mainView = myApp.addView('.view-main');
        //console.log(mainView.activePage.name);
        if(mainView.activePage.name==='index')
        {
                if(alert_is_shown===false)
                {
                    alert_is_shown = true;
                    myApp.confirm('آیا مایل به خروج هستید؟' ,'خروج',
                        function () {
                            alert_is_shown = false;
                            navigator.app.exitApp();
                        },
                        function(){
                            alert_is_shown = false;
                        }
                    );
                }
        }
        else if(mainView.activePage.name==='manifest_det')
            mainView.loadPage('index.html');
        else if(mainView.activePage.name==='form')
        {
            console.log('back from "form.html" and executing getManifestDet(); ....');

            mainView.loadPage('manifest_det.html');
        }
        else
            mainView.goBack();   
    }
}
function exitApp()
{
    myApp.confirm('آیا مایل به خروج هستید؟' ,'خروج',
        function () {
            navigator.app.exitApp();
        }
    );
}
function monize2(inp){
    var out;
    var sht=String(inp).replace(/,/gi,'');
    var txt = sht.split('');
    var j=-1;
    var tmp='';
    for(var i=txt.length-1;i>=0;i--){
        if(j<2){
            j++;
            tmp=txt[i]+tmp;
        }else{
            j=0;
            tmp=txt[i]+','+tmp;
        }
    out=tmp;
    }
    return(out);
}
function umonize(inp){
    var out='0';
    var sht=inp.replace(/,/gi,'');
    sht=sht.replace(/\./gi,'');
    out=sht;
    return(out);
}
function monize(obj){
    var sht=String(obj.value).replace(/,/gi,'');
    var txt = sht.split('');
    var j=-1;
    var tmp='';
    for(var i=txt.length-1;i>=0;i--){
            //alert(txt[i]);
            if(j<2){
                    j++;
                    tmp=txt[i]+tmp;
            }else{
                    j=0;
                    tmp=txt[i]+','+tmp;
            }
    }
    obj.value = tmp;
}
var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    receivedEvent: function(id) {
       if(id==='deviceready')
           intial();
    }
};

app.initialize();

function intial(){
    db_bussy = true;
    db = window.openDatabase("gohar", "1.0", "Gohar DB", 1000000);
    createTable();
    //startSql();
    //db.transaction(createTable, errorCB, successCB);

    
    $( document ).ajaxError(function( event, request, settings ) {
        myApp.alert("خطا در دسترسی به سرور لطفا اتصال شبکه را بررسی و منتظر اتصال نرم افزار باشید",alert_head);
        console.log(event,request,settings);
    });
    
    setTimeout(function(){
            conf_get("run_count",function(d){
                var run_count = 0;
                if(d.id > 0)
                    run_count = parseInt(d.value,10);
                else
                    window.plugins.Shortcut.CreateShortcut("Manifest", function(){}, function(){} );
                conf_add("run_count",run_count+1);
            });
        loadUser(function(){
            if($.trim(user)==='')
            {
                hs_login_panel = true;
                openLoginDialog();
            }
            else
            {
                getManifest();
                //console.log('start senStats()...');
                //net_stat_send_enabled = true;
                sendStats_once();
            }
        });
        //expiretimer();
        //updateApp();
    },2000);
    myApp = new Framework7({
        swipePanel: 'left'
    });
    //myApp=new Framework7();
    $$=Dom7;
    mainView=myApp.addView('.view-main',{dynamicNavbar:true});
    myApp.onPageInit('index',function(page){
        getManifest();
    });
    myApp.onPageInit('manifest_close',function(page){
        getManifest_close();
    });
    myApp.onPageInit('manifest_det',function(page){
        getManifestDet();
        setEnterEvent();
        hs_update_manifest_det = true;
        check_unsync(hs_manifest_det['id']);
        //$("#search").focus();
    });
    /*
    $$('.pull-to-refresh-content').on('refresh',function(e){
        console.log('Pull to refresh...');
    });
    */
    $$('.panel-left').on('opened', function () {
        side_open = true;
    });
    $$('.panel-left').on('closed', function () {
        side_open = false;
    });
    document.addEventListener("online", onOnline, false);
    
}
function close_manifest(manifest_id)
{
    myApp.confirm('آیا بستن انجام شود؟',alert_head,function(){
        ex_sql("select id from manifest_det where manifest_id="+manifest_id+" and is_sync=0",function(data,a){
            if(data.length !== 0)
            {
                myApp.alert("این لیست به سرور ارسال نشده است",alert_head);
                return false;  
            }
            var man = new manifest();
            man.construct(manifest_id,function(man){
                ob={
                    "user":user,
                    "pass":md5(pass),
                    "data":{
                        "close":
                        [
                            {
                                "f_number":man.f_number,
                                "f_date":man.f_date
                            }
                        ]
                    }
                };
                $.get(manifest_url,ob,function(result){
                    console.log(result);
                    try {
                        var tt = JSON.parse(result);
                        
                        updateManifest();
                        /*
                        console.log(tt);
                        for(var i =0;i<tt.data.length;i++)
                        {
                            if(parseInt(tt.data[i].f_number,10)===parseInt(man.f_number,10) && tt.data[i].f_date===man.f_date && $.trim(tt.data[i].close)!=='')
                            {
                                console.log('ready to delete');
                                var man1 = new manifest();
                                man1.load(parseInt(tt.data[i].f_number,10),parseInt(man.f_number,10),function(man1){
                                    man1.delete(function(){
                                        console.log('deleted');
                                        getManifest_close();
                                        updateManifest();
                                        console.log('view refresh');
                                    });
                                });
                            }    
                        }   
                        */
                    }
                    catch (e)
                    {
                        myApp.alert("خطا در بستن مانیفست",alert_head);
                    }
                });     
            });
            
            
        });
    });
}
function check_unsync(manifest_id)
{
    ex_sql("select id from manifest_det where manifest_id="+manifest_id+" and is_sync=0",function(data,a){
        if(data.length>0)
           $("#update_par").show();
        else
           $("#update_par").hide();
    });
}
