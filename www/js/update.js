var update_server = "http://gcom.ir/android/main/";
//var update_server = "http://192.168.1.80/update.json";
function updateApp(showAlert)
{
    console.log('start update');
    cordova.getAppVersion(function (version) {
        console.log("version :",version);
        var p = {
            "app" : "gohar_jimbo",
            "version" : version,
            "uuid" : device.uuid
        };
        console.log(p);
        $.getJSON(update_server,p,function(res){
            console.log(res);
            if((parseInt(res.en,10)===1 || parseInt(res.en,10)===2) && res.version > p.version)
            {
                if(parseInt(res.en,10)===1)
                    if(!confirm("آیا بروزرسانی نسخه جدید انجام شود؟"))
                        return(false);
                $("body").prepend("<div id='overl' style='vertical-align:middle;width:100%;height:100%;background:#000000;color:#ffffff;poistion:absolute;top:0px,left:0px;'></div>");
                var overl = '';
                overl += '<div><h1 align="center">نسخه '+res.version+' نرم افزار در حال بروزرسانی می باشد</h1><hr/>';
                overl += '<h3 align="center"><span class="update_prg">0%</span></h3></div>';
                $("#overl").html(overl);
                /*
                if($.trim(res.query) !== '')
                {
                    alert('wait for db start');
                    while(db_bussy === true)
                    {
                        
                    }
                    alert("start query");
                    ex_sql("insert into reserves (tarikh,saat,voucher_id,refr,en,mabda,maghsad,sname) values ('1393-09-30','06:00','2400','0','1','tehran','mhd','mirsamie')",function(a,b){
                        alert('query DONE');
                        $("#overl").remove();
                        downloadUrl(res.url,"cdvfile://localhost/persistent/1.apk",function(done,total){
                            $(".update_prg").html(String(parseInt(done*100/total,10))+"%");
                        },function(furl){
                            //myApp.alert("بروزرسانی با موفقیت در آدرس "+furl+" انجام شد",alert_head,function(){
                            myApp.alert("بروزرسانی با موفقیت انجام شد",alert_head,function(){
                                $("#overl").remove();
                                installApk.install(furl);
                                navigator.app.exitApp();
                            });
                        },function(errorObj){
                            myApp.alert("حطا در بروزرسانی",alert_head);
                            console.log(errorObj);
                            $("#overl").remove();
                        });
                    });
                }
                else
                {
                */
                    downloadUrl(res.url,"cdvfile://localhost/persistent/1.apk",function(done,total){
                        $(".update_prg").html(String(parseInt(done*100/total,10))+"%");
                    },function(furl){
                        myApp.alert("بروزرسانی با موفقیت در آدرس "+furl+" انجام شد",alert_head,function(){
                            $("#overl").remove();
                            installApk.install(furl);
                            navigator.app.exitApp();
                        });
                    },function(errorObj){
                        myApp.alert("حطا در بروزرسانی",alert_head);
                        console.log(errorObj);
                        $("#overl").remove();
                    });                    
                //}
            }
            else if(parseInt(res.en,10)===0)
                stopProgram(res.msg);
            else if(showAlert===true)
                myApp.alert('نسخه جدیدی پیدا نشد',alert_head);
        });        
    });
}
function downloadUrl(url,des,progressFn,successFn,errorFn)
{

    var fileTransfer = new FileTransfer();
    var uri = encodeURI(url);
    fileTransfer.onprogress = function(progressEvent) {
        /*
        if (progressEvent.lengthComputable)
            console.log('compulatble');
        else
            console.log('not compulatble');
        console.log(progressEvent.loaded , progressEvent.total);
        */
        if(typeof progressFn === 'function')
            progressFn(progressEvent.loaded,progressEvent.total);
    };
    fileTransfer.download(
        uri,
        des,
        function(entry) {
            console.log("download complete: " + entry.toURL());
            if(typeof successFn === 'function')
                successFn(entry.toURL());
        },
        function(error) {
            console.log("download error source " + error.source);
            console.log("download error target " + error.target);
            console.log("upload error code" + error.code);
            if(typeof errorFn === 'function')
                errorFn(error);
        },
        false,
        {
            headers: {
                "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
            }
        }
    );


}
